

const tabs = document.querySelectorAll(".tabs-title");
const tabsContent = document.querySelector(".tabs-content");
const tabsContentChild = tabsContent.children;

const tabsArray= [...tabs];
const tabsContentArray = [...tabsContentChild];

function tabsContentRemove(arr){
    arr.forEach((element) => {
        element.style.display ="none"
    });
}

function defaltState(){
    tabsContentRemove(tabsContentArray);

tabsArray.forEach((element)=>{
    element.classList.remove("active");
});
}

defaltState();


tabsArray.forEach((element, index) => {
    element.addEventListener("click", () => {
        tabsArray.forEach((element) =>{
            element.classList.remove("active");
        });
        tabsContentRemove(tabsContentArray);
        element.classList.add("active");
        tabsContentArray[index].style.display = "block";
    });
});




